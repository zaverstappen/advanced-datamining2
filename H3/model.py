from collections import Counter
from copy import deepcopy
import math
import random

# Activation functions
 # Linear function
def linear(a):
    return a

 # Signum function
def sign(a):
    if a > 0:
        yhat = 1.0
    elif a < 0:
        yhat = -1.0
    else:
        yhat = 0
    return yhat

 # Smoothed sigmoid function(TANH)
def tanh(a):
    return math.tanh(a)


# Loss functions
 # Quadratic loss fucntion
def mean_squared_error(yhat, y):
    return (yhat-y)**2

 # Absolute loss function
def mean_absolute_error(yhat, y):
    return abs(yhat-y)

 # Hinge loss function
def hinge(yhat, y):
    return max(1-(yhat*y), 0)


# Differentiation function
def derivative(function, delta=0.001):

    def wrapper_derivative(x, *args):
        return (function(x+delta, *args) - function(x-delta, *args)) / (2*delta)  # Hier kun je o.a. "function(x)" aanroepen om de afgeleide mee te berekenen

    wrapper_derivative.__name__ = function.__name__ + '’'
    wrapper_derivative.__qualname__ = function.__qualname__ + '’'
    return wrapper_derivative
               

# Class Perceptron
class Perceptron():

    def __init__(self, dim):
        self.dim = dim
        self.bias = 0.0
        self.weights = [0.0 for i in range(dim)]
    
    
    def __repr__(self):
        text = f'Perceptron(dim={self.dim})'
        return text
    
    
    def predict(self, xs):
        yhats = []
        for i in range(len(xs)):
            a = self.bias
            for d in range(self.dim):
                a += self.weights[d] * xs[i][d]
            if a > 0:
                yhat = 1.0
            elif a < 0:
                yhat = -1.0
            else:
                yhat = 0
            yhats.append(yhat)
        return yhats
    
    
    def partial_fit(self, xs, ys):
        yhats = self.predict(xs)
        for x, y, yhat in zip(xs, ys, yhats):
            e = yhat-y
            self.bias -= e
            for i in range(self.dim):
                self.weights[i] -= e*x[i]
                
                
    def fit(self, xs, ys, *, epochs=0):
        if epochs != 0:
            for epoch in range(epochs):
                self.partial_fit(xs, ys)
                yhats = self.predict(xs)
                if yhats == ys:
                    break
        else:
            corrects = False
            while not corrects:
                self.partial_fit(xs, ys)
                yhats = self.predict(xs)
                if yhats == ys:
                    corrects = True
            
            
# Class Linear Regression 
class LinearRegression():

    def __init__(self, dim):
        self.dim = dim
        self.bias = 0.0
        self.weights = [0.0 for i in range(dim)]
    
    
    def __repr__(self):
        text = f'LinearRegression(dim={self.dim})'
        return text
    
    
    def predict(self, xs):
        yhats = []
        for i in range(len(xs)):
            yhat = self.bias
            for d in range(self.dim):
                yhat += self.weights[d] * xs[i][d]
            yhats.append(yhat)
        return yhats
    
    
    def partial_fit(self, xs, ys, *, alpha=0.001):
        yhats = self.predict(xs)
        for x, y, yhat in zip(xs, ys, yhats):
            e = yhat-y
            self.bias -= alpha*e
            for i in range(self.dim):
                self.weights[i] -= alpha*e*x[i]
                
                
    def fit(self, xs, ys, *, alpha=0.001, epochs=40):
        for epoch in range(epochs):
            self.partial_fit(xs, ys, alpha=alpha)    
    
# Class Neuron
class Neuron():
    
    def __init__(self, dim, activation=linear, loss=mean_squared_error):
        self.dim = dim
        self.a = []
        self.activation = activation
        self.loss = loss
        self.bias = 0.0
        self.weights = [0.0 for i in range(dim)]

        
    def __repr__(self):
        text = f'Neuron(dim={self.dim}, activation={self.activation.__name__}, loss={self.loss.__name__})'
        return text
       
    
    def predict(self, xs):
        yhats = []
        for i in range(len(xs)):
            a = self.bias
            for d in range(self.dim):
                a += self.weights[d]*xs[i][d]
            self.a.append(a)
            yhat = self.activation(a)
            yhats.append(yhat)
        return yhats
    
    def partial_fit(self, xs, ys, *, alpha=0.001):
        yhats = self.predict(xs)
        derivative_loss = derivative(self.loss)
        derivative_activation = derivative(self.activation)
        for x, y, yhat, a in zip(xs, ys, yhats, self.a):
            for i in range(self.dim):
                self.bias -= alpha*derivative_loss(yhat, y)*derivative_activation(a)
                self.weights[i] -= alpha*derivative_loss(yhat, y)*derivative_activation(a)*x[i]
                
                
    def fit(self, xs, ys, *, alpha=0.001, epochs=40):
        for epoch in range(epochs):
            self.partial_fit(xs, ys, alpha=alpha)
        
# Class Layer        
class Layer():

    classcounter = Counter()

    def __init__(self, outputs, *, name=None, next=None):
        Layer.classcounter[type(self)] += 1
        if name is None:
            name = f'{type(self).__name__}_{Layer.classcounter[type(self)]}'
        self.inputs = 0
        self.outputs = outputs
        self.name = name
        self.next = next

    def __repr__(self):
        text = f'Layer(inputs={self.inputs}, outputs={self.outputs}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text
    
    def __add__(self, next):
        result = deepcopy(self)
        result.add(deepcopy(next))
        return result
    
    def __getitem__(self, index):
        if index == 0 or index == self.name:
            return self
        if isinstance(index, int):
            if self.next is None:
                raise IndexError('Layer index out of range')
            return self.next[index - 1]
        if isinstance(index, str):
            if self.next is None:
                raise KeyError(index)
            return self.next[index]
        raise TypeError(f'Layer indices must be integers or strings, not {type(index).__name__}')
        
    def __call__(self, xs):
        raise NotImplementedError('Abstract __call__ method')

        
    def add(self, next):
        if self.next is None:
            self.next = next
            next.set_inputs(self.outputs)
        else:
            self.next.add(next)

    def set_inputs(self, inputs):
        self.inputs = inputs
        
        
        
# Class InputLayer
class InputLayer(Layer):

    def __repr__(self):
        text = f'InputLayer(outputs={self.outputs}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text
    
    def __call__(self, xs, ys=None, alpha=None):
        yhats, ls , gs = self.next(xs, ys, alpha)
        return yhats, ls, gs
    
    def set_inputs(self, inputs):
        raise NotImplementedError("Cannot be connected to previous layer.")

    def predict(self, xs):
        yhats, _, _ = self(xs)
        return yhats
    
    def evaluate(self, xs, ys):
        _, ls, _ = self(xs, ys)
        lmean = sum(ls) / len(ls)
        return lmean
    
    def partial_fit(self, xs, ys, alpha=0.001):
        self(xs, ys, alpha)
        
    def fit(self, xs, ys, alpha=0.001, epochs=40):
        for epoch in range(epochs):
            self.partial_fit(xs, ys, alpha)

    
# Class DenseLayer
class DenseLayer(Layer):
    
    def __init__(self, outputs, name=None):
        super().__init__(outputs, name=name, next=None)
        self.bias = [0.0 for o in range(self.outputs)]
        self.weights = [[] for i in range(self.outputs)]
        
    def __repr__(self):
        text = f'DenseLayer(inputs={self.inputs}, outputs={self.outputs}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text
    
    def __call__(self, xs, ys=None, alpha=None):
        gs = None
        aa = [[self.bias[o] + sum(self.weights[o][i] * x[i] for i in range(self.inputs)) for o in range(self.outputs)] for x in xs]
        yhats, ls, qs = self.next(aa, ys, alpha)
        if alpha is not None:  
            for q, x in zip(qs, xs):
                for o in range(self.outputs):
                    self.bias[o] -= (alpha/len(xs)) * q[o]
                    for i in range(self.inputs):
                        self.weights[o][i] -= (alpha/len(xs)) * q[o] * x[i]
            gs = [[sum( q[o] * self.weights[o][i] for o in range(self.outputs)) for i in range(self.inputs)] for q in qs]
        return yhats, ls, gs

    
    def set_inputs(self, inputs):
        super().set_inputs(inputs)
        spread = math.sqrt( 6/ (self.inputs+self.outputs))
        self.weights = [[random.uniform(-spread, spread) for i in range(self.inputs)] for o in range(self.outputs)]
        
        
        
# Class ActivationLayer
class ActivationLayer(Layer):

    def __init__(self, outputs, activation=linear, name=None):
        super().__init__(outputs, name=name, next=None)
        self.activation = activation
        self.derirative_activation = derivative(activation)

    def __repr__(self):
        text = f'ActivationLayer(inputs={self.inputs}, outputs={self.outputs}, activation={self.activation.__name__}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text
    
    def __call__(self, xs, ys=None, alpha=None):
        gs = None
        hh = [[self.activation(x[o]) for o in range(self.outputs)] for x in xs]
        yhats, ls, qs = self.next(hh, ys, alpha)
        if alpha is not None:
            gs = [[ q[o]*self.derirative_activation(x[o]) for o in range(self.outputs)] for x, q in zip(xs, qs)]
        return yhats, ls, gs
    
    
# Class LossLayer
class LossLayer(Layer):
    
    def __init__(self, loss=mean_squared_error, name=None):
        super().__init__(0, name=name, next=None)
        self.loss = loss
        self.derirative_loss = derivative(loss)
        
    def __repr__(self):
        text = f'LossLayer(inputs={self.inputs}, loss={self.loss.__name__}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text
    
    def __call__(self, xs, ys=None, alpha=None):
        ls = None
        gs = None
        yhats = xs
        if ys is not None:
            ls = [sum(self.loss(x[i], y[i]) for i in range(self.inputs)) for x, y in zip(xs, ys)]
            if alpha is not None:
                gs = [[ self.derirative_loss(x[i], y[i]) for i in range(self.inputs)] for x, y in zip(xs, ys)]
        return yhats, ls, gs
    
    def add(self, next):
        raise NotImplementedError("Cannot add extra layer.")

    
        
       