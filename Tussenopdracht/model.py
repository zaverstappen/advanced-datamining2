import math

# Activation functions
 # Linear function
def linear(a):
    return a

 # Signum function
def sign(a):
    if a > 0:
        yhat = 1.0
    elif a < 0:
        yhat = -1.0
    else:
        yhat = 0
    return yhat

 # Smoothed sigmoid function(TANH)
def tanh(a):
    return math.tanh(a)


# Loss functions
 # Quadratic loss fucntion
def mean_squared_error(yhat, y):
    return (yhat-y)**2

 # Absolute loss function
def mean_absolute_error(yhat, y):
    return abs(yhat-y)

 # Hinge loss function
def hinge(yhat, y):
    return max(1-(yhat*y), 0)


# Differentiation function
def derivative(function, delta=0.001):

    def wrapper_derivative(x, *args):
        return (function(x+delta, *args) - function(x-delta, *args)) / (2*delta)  # Hier kun je o.a. "function(x)" aanroepen om de afgeleide mee te berekenen

    wrapper_derivative.__name__ = function.__name__ + '’'
    wrapper_derivative.__qualname__ = function.__qualname__ + '’'
    return wrapper_derivative
               

# Class Perceptron
class Perceptron():

    def __init__(self, dim):
        self.dim = dim
        self.bias = 0.0
        self.weights = [0.0 for i in range(dim)]
    
    
    def __repr__(self):
        text = f'Perceptron(dim={self.dim})'
        return text
    
    
    def predict(self, xs):
        yhats = []
        for i in range(len(xs)):
            a = self.bias
            for d in range(self.dim):
                a += self.weights[d] * xs[i][d]
            if a > 0:
                yhat = 1.0
            elif a < 0:
                yhat = -1.0
            else:
                yhat = 0
            yhats.append(yhat)
        return yhats
    
    
    def partial_fit(self, xs, ys):
        yhats = self.predict(xs)
        for x, y, yhat in zip(xs, ys, yhats):
            e = yhat-y
            self.bias -= e
            for i in range(self.dim):
                self.weights[i] -= e*x[i]
                
                
    def fit(self, xs, ys, *, epochs=0):
        if epochs != 0:
            for epoch in range(epochs):
                self.partial_fit(xs, ys)
                yhats = self.predict(xs)
                if yhats == ys:
                    break
        else:
            nepoch = 0
            corrects = False
            while not corrects:
                self.partial_fit(xs, ys)
                yhats = self.predict(xs)
                nepoch += 1
                if yhats == ys:
                    corrects = True
                    print(nepoch)
            
            
# Class Linear Regression 
class LinearRegression():

    def __init__(self, dim):
        self.dim = dim
        self.bias = 0.0
        self.weights = [0.0 for i in range(dim)]
    
    
    def __repr__(self):
        text = f'LinearRegression(dim={self.dim})'
        return text
    
    
    def predict(self, xs):
        yhats = []
        for i in range(len(xs)):
            yhat = self.bias
            for d in range(self.dim):
                yhat += self.weights[d] * xs[i][d]
            yhats.append(yhat)
        return yhats
    
    
    def partial_fit(self, xs, ys, *, alpha=0.001):
        yhats = self.predict(xs)
        for x, y, yhat in zip(xs, ys, yhats):
            e = yhat-y
            self.bias -= alpha*e
            for i in range(self.dim):
                self.weights[i] -= alpha*e*x[i]
                
                
    def fit(self, xs, ys, *, alpha=0.001, epochs=40):
        for epoch in range(epochs):
            self.partial_fit(xs, ys, alpha=alpha)    
    
    
# Class Neuron
class Neuron():
    
    def __init__(self, dim, activation=linear, loss=mean_squared_error):
        self.dim = dim
        self.activation = activation
        self.loss = loss
        self.bias = 0.0
        self.weights = [0.0 for i in range(dim)]

        
    def __repr__(self):
        text = f'Neuron(dim={self.dim}, activation={self.activation.__name__}, loss={self.loss.__name__})'
        return text
    
    def predict(self, xs):
        yhats = []
        for i in range(len(xs)):
            a = self.bias
            for d in range(self.dim):
                a += self.weights[d]*xs[i][d]
            yhat = self.activation(a)
            yhats.append(yhat)
        return yhats
    
    def partial_fit(self, xs, ys, *, alpha=0.001):
        yhats = self.predict(xs)
        derivative_loss = derivative(self.loss)
        derivative_activation = derivative(self.activation)
        for x, y, yhat in zip(xs, ys, yhats):
            for i in range(self.dim):
                self.bias -= alpha*derivative_loss(yhat, y)*derivative_activation(x[i])
                self.weights[i] -= alpha*derivative_loss(yhat, y)*derivative_activation(x[i])*x[i]
                
                
    def fit(self, xs, ys, *, alpha=0.001, epochs=40):
        for epoch in range(epochs):
            self.partial_fit(xs, ys, alpha=alpha)
        
        
    
    